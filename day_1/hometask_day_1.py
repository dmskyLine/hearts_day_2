def get_symbol_frequency(text: str) -> dict:
    """The function converts a given string into a dictionary
    in which KEYS are the SYMBOLS of the string and VALUES
    are relevant FREQUENCIES of the symbols.
    :param text: given string
    :return: dictionary(key -> symbol; value -> frequency of the symbol)
    """
    out_dict: dict = {}
    total: int = len(text)

    for symbol in set(text):
        out_dict[symbol] = round((text.count(symbol) / total * 100), 2)
    return out_dict


def get_ternary(x, y):
    """The function gets params x and y and following logic is interpreted
    using ternary operator:
    x < y -> x + y
    x == y -> 0
    x > y -> x - y
    x == 0 and y == 0 -> "game over"
    """
    return str(
        x + y
        if (x < y)
        else x - y
        if (x > y)
        else "game over"
        if (x == 0 and y == 0)
        else 0
    )


if __name__ == "__main__":
    cases = ((1, 2, "3"), (1, 1, "0"), (2, 1, "1"), (0, 0, "game over"))
    for x, y, result in cases:
        func_res = get_ternary(x, y)
        assert (
            func_res == result
        ), f"ERROR: get_ternary({x}, {y}) returned {func_res} but expected: {result}"

    situations = (("", {}), ("Hello", {"H": 20.0, "e": 20.0, "l": 40.0, "o": 20.0}))
    for s, res in situations:
        output = get_symbol_frequency(s)
        assert (
            output == res
        ), f"ERROR: get_symbol_frequency({s}) returned {output} but expected: {res}"
